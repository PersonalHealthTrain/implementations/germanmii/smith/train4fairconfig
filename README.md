### Train4FAIR quick start installation guidelines
This repository contains the source codes for the train modelling tools, an approach based on FAIR princeiple, which is a part of Personal Health Train implementation.
Refer to http://train4fair.org for more details. 

### Pre-requirements
**Add the following lines to the top of the /etc/hosts**

```
0.0.0.0       train.platform.de
::            train.platform.de
```
Following is the list of supported environments:
* cloud41 (cloud41.dbis.rwth-aachen.de)
* localhost (127.0.0.1 and cloud41)
* localtotal (127.0.0.1)
* menzel (menzel.informatik.rwth-aachen.de)
* train4fair_com (train4fair.com)
* train4fair_de (train4fair.de)

### Services installation (custom mode)
1. `sudo wget http://gofair:gofair@167.172.175.112:9997/installation/development/{environment_name}/installation.tar.gz`
2. `sudo tar xvzf installation.tar.gz`
3. `cd installation`
4. `sudo chmod 777 train_install.sh`
5. `sudo ./train_install.sh --init true`

### Client installations
Client servic can be installed in two ways: i) locally, ii) using Docker. 

##### Standard mode: 
1. `sudo apt install nodejs`
2. `sudo apt install npm`
3. `sudo git clone https://github.com/Train4FAIR/train4fair-client-dashboard.git`
4. `cd train4fair-client-dashboard`
5. `sudo ./start.sh --init true --env dev`
6. `sudo ./start.sh --refresh true --env cloud41`

**Note: The clients could run without to be necessary the services. By default, they are already pointing to the cloud services.**

##### Docker mode: 
1. `sudo apt install docker-ce`
2. `sudo apt install docker-compose`
3. `sudo wget https://raw.githubusercontent.com/Train4FAIR/train4fair-client-dashboard/master/packages/node_modules/node-red/client-installation/docker-compose.yml`
4. `sudo docker-compose up`

**Note: The clients could run without to be necessary the services. By default they are already pointing to the cloud services.**

#### Getting Help
Keep in touch with us by the Train4FAIR Forum [https://groups.google.com/forum/#!forum/train4fair](https://groups.google.com/forum/#!forum/train4fair).

#### Contributing
Please file issues or pull-requests(PR). Howver, before raising a PR, please read our [contributing guide](https://groups.google.com/forum/#!forum/train4fair](https://groups.google.com/forum/#!forum/train4fair).
This project adheres to the [Contributor Covenant 1.4](http://contributor-covenant.org/version/1/4/). By participating, you are expected to uphold this code. Please report unacceptable behavior to any of the project's core team at team@nodered.org.

#### Authors
Train4FAIR is a project of the [Fraunhofer FIT](https://www.fit.fraunhofer.de/) and RWTH](rwth-aachen.de) and developed by **João Bosco Jares** [@jbjares](http://twitter.com/jbjares) (Train4FAIR)

#### Copyright and license
Copyright of Fraunhofer FIT and RWTH Project under [the Apache 2.0 license](LICENSE).
